<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>
	<!-- If jps page contain any spring/jstl/custom tag then that jsp page should be accessed with controller -->

	<!-- Default method type of Spring form tag is "POST" -->
	<!-- Default method type of HTML form tag is "GET" -->

	<!-- If we don't mention modelAttribute parameter here then Spring container will by default try to find object for binding data by the name of "command" -->
	<springForm:form method="post" action="auth_modelMap"
		modelAttribute="login">
		Email: <springForm:input type="text" path="email" />
		<br /> Password: <springForm:input type="password" path="password" />
		<br />
		<input type="submit" value="Sign In" />
	</springForm:form>
</body>
</html>

