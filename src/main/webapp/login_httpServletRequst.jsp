<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>
	<!-- If jps page contain any spring/jstl/custom tag then that jsp page should be accessed with controller -->

	<!-- Default method type of Spring form tag is "POST" -->
	<!-- Default method type of HTML form tag is "GET" -->

	<!-- If we don't mention modelAttribute parameter here then Spring container will by default try to find object for binding data by the name of "command" -->
	<form method="post" action="auth_httpServletRequst">
		Email: <input type="text" name="email" /> <br /> Password: <input
			type="password" name="password" /> <br /> <input type="submit"
			value="Sign In" />
	</form>
</body>
</html>

