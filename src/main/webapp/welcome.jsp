<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>

	<!-- This JSP page is not having any spring tag so this jsp page can be directly accessed without any controller -->

	<!-- If jps page contain any spring/jstl/custom tag then that jsp page should be accessed with controller -->

	<h1>Welcome to Spring MVC</h1>

	<a href="login_httpServletRequst.jsp"> Login Page : form data
		binding by HttpServletRequest object in method parameter [Only
		View->Controller]</a>
	<br>
	<br>
	
	<a href="login_RequestParamAnnotation.jsp"> Login Page : form data
		binding by @RequestParam annotation in method parameter [Only
		View->Controller]</a>
	<br>
	<br>

	<a href="login_className"> Login Page : form data binding by Direct
		Login(Model) name in method parameter [Both Controller->View and
		View->Controller]</a>
	<br>
	<br>

	<a href="login_ModelAttributeAnnotation"> Login Page : form data binding by
		@ModelAttribute annotation in method parameter [Both Controller->View
		and View->Controller]</a>

	<br>
	<br>
	<a href="login_model"> Login Page : form data binding by Model
		Interface in method parameter [Only Controller->View]</a>

	<br>
	<br>
	<a href="login_modelMap"> Login Page : form data binding by
		ModelMap Class in method parameter [Only Controller->View]</a>

	<br>
	<br>
	<a href="login_Map"> Login Page : form data binding by
		Map(java.util.Map) Interface in method parameter [Only
		Controller->View]</a>

	<br>
	<br>
	<a href="login_modelAndView"> Login Page : form data binding by
		ModelAndView Class as method return type [Only Controller->View]</a>


</body>
</html>

