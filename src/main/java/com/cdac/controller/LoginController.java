package com.cdac.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cdac.model.Login;

@Controller
public class LoginController {

	/*
	 * As login.jsp page contains spring form tag, it is necessary to send a
	 * model/object of login class to jsp page
	 * 
	 * We can do this in 2 ways
	 * 
	 * 1. Explicitly creating and adding object in model
	 * 
	 * 2. Use @ModelAttribute annotation and let Spring create and add that object
	 * to jsp page
	 * 
	 * In JSP page we have to access the login object by its Class Name i.e.
	 * modelAttribute="login" (camelCase convention)
	 * 
	 * Note : Convention for modelAttribute spelling :
	 * 
	 * | Model Class Name | modelAttribute Name
	 * 
	 * 1. Login -> login
	 * 
	 * 2. LoginPage -> loginPage
	 * 
	 * Otherwise we can give custome name
	 */

	/*
	 * Model in spring is abtraction on reuqest(HttpServletRequest) object
	 * 
	 * Dispatcher servlet adds object in model map, internally this is nothing but
	 * request.addAttribute(key,value)
	 * 
	 * Hence scope of model is limited to request scope, once response is sent to
	 * client model object gets discarded/garbage collected
	 */

	/*
	 * Ways of passing model object from `Controller to View` i.e. For GET Method
	 */

	/*
	 * Way 0 : In JSP Page write java code and set default values for form field
	 * parameters
	 * 
	 * We can use JSP Action tags scriplets or expression for this purpose
	 * 
	 * But this is not recommended way
	 */
	/*
	 * Not implemented here
	 */
	@RequestMapping(value = "/login_javaCodeInJSP", method = RequestMethod.GET)
	public String loginPage1() {
		return "login_javaCodeInJSP";
	}

	/*
	 * Way 1 : Using direct Login(Model) Class name in method parameter
	 */
	@RequestMapping(value = "/login_className", method = RequestMethod.GET)
	public String loginPage1(Login l) {
		l.setEmail("default@cdac.in");
		l.setPassword("default@cdac.in");
		return "login_className";
	}

	/*
	 * Way 2 : Using @ModelAttribute annotation in method parameter
	 */
	@RequestMapping(value = "/login_ModelAttributeAnnotation", method = RequestMethod.GET)
	public String loginPage(@ModelAttribute("login") Login l) {
		l.setEmail("default@cdac.in");
		l.setPassword("default@cdac.in");
		return "login_modelAttribute";
	}

	/*
	 * Way 3 : Using Model Interface in method parameter
	 */
	@RequestMapping(value = "/login_model", method = RequestMethod.GET)
	public String loginPage(Model model) {
		Login l = new Login();
		l.setEmail("default@cdac.in");
		l.setPassword("default@cdac.in");

		/*
		 * Here we give custom modelAttribute name as "login" we can give any other name
		 * just make sure that in jsp page modelAttribute parameter in form tag should
		 * have same value
		 */

		model.addAttribute("login", l);

		return "login_model";
	}

	/*
	 * Way 4 : Using ModelMap class in method parameter
	 */
	@RequestMapping(value = "/login_modelMap", method = RequestMethod.GET)
	public String loginPage(ModelMap modelmap) {
		Login l = new Login();
		l.setEmail("default@cdac.in");
		l.setPassword("default@cdac.in");

		/*
		 * Here we give custom modelAttribute name as "login" we can give any other name
		 * just make sure that in jsp page modelAttribute parameter in form tag should
		 * have same value
		 */

		modelmap.addAttribute("login", l);

		return "login_modelMap";
	}

	/*
	 * Way 5 : Using Map(java.util.Map) Interface in method parameter
	 */
	@RequestMapping(value = "/login_Map", method = RequestMethod.GET)
	public String loginPage(Map<String, Object> map) {
		Login l = new Login();
		l.setEmail("default@cdac.in");
		l.setPassword("default@cdac.in");

		/*
		 * Here we give custom modelAttribute name as "login" we can give any other name
		 * just make sure that in jsp page modelAttribute parameter in form tag should
		 * have same value
		 */

		map.put("login", l);

		return "login_Map";
	}

	/*
	 * Way 6 : Using ModelAndView as return type of Method
	 */
	@RequestMapping(value = "/login_modelAndView", method = RequestMethod.GET)
	public ModelAndView loginPage() {
		Login l = new Login();
		l.setEmail("default@cdac.in");
		l.setPassword("default@cdac.in");

		/*
		 * Here we give custom modelAttribute name as "login" we can give any other name
		 * just make sure that in jsp page modelAttribute parameter in form tag should
		 * have same value
		 */

		ModelAndView modelAndView = new ModelAndView("login_modelAndView", "login", l);

		return modelAndView;
	}

	/*
	 * Ways of passing model object from `View to Controller` i.e. For POST Method
	 */

	/*
	 * Way 1 : Using plain HttpServletRequest object and get request parameters from
	 * request object
	 */

	@RequestMapping(value = "/auth_httpServletRequst", method = RequestMethod.POST)
	public String authenticate(HttpServletRequest httpRequest) {
		String email = httpRequest.getParameter("email");
		String password = httpRequest.getParameter("password");
		if (email.equals(password))
			return "welcome";
		else
			return "failed";
	}

	/*
	 * Way 2 : Using @RequestParam annotation in method parameter
	 */

	@RequestMapping(value = "/auth_RequestParamAnnotation", method = RequestMethod.POST)
	public String authenticate(@RequestParam("email") String email, @RequestParam("password") String password) {
		if (email.equals(password))
			return "welcome";
		else
			return "failed";
	}

	/*
	 * Way 3: Using direct Login(Model) Class name in method parameter
	 */

	@RequestMapping(value = "/auth_className", method = RequestMethod.POST)
	public String authenticate(Login l) {
		if (l.getEmail().equals(l.getPassword()))
			return "welcome";
		else
			return "failed";
	}

	/*
	 * Way 3 : Using @ModelAttribute annotation in method parameter
	 */
	@RequestMapping(value = "/auth_ModelAttributeAnnotation", method = RequestMethod.POST)
	public String authenticate1(@ModelAttribute("login") Login l) {
		if (l.getEmail().equals(l.getPassword()))
			return "welcome";
		else
			return "failed";
	}

	/*
	 * In case of Model Interface name as method parameter in post method we never
	 * get request parameters in model object
	 */

	@RequestMapping(value = "/auth_model", method = RequestMethod.POST)
	public String authenticate(Model model) {
		System.out.println(model); // Output : {}

		String email = (String) model.getAttribute("email"); // email is null
		String password = (String) model.getAttribute("password"); // password is null

		if (email.equals(password)) // We will get java.lang.NullPointerException
			return "welcome";
		else
			return "failed";
	}

	/*
	 * In case of ModelMap Class name as method parameter in post method we never
	 * get request parameters in model map
	 */

	@RequestMapping(value = "/auth_modelMap", method = RequestMethod.POST)
	public String authenticate(ModelMap modelMap) {
		System.out.println(modelMap); // Output : {}

		String email = (String) modelMap.getAttribute("email"); // email is null
		String password = (String) modelMap.getAttribute("password"); // password is null

		if (email.equals(password)) // We will get java.lang.NullPointerException
			return "welcome";
		else
			return "failed";
	}

	/*
	 * In case of Map Interface name as method parameter in post method we never get
	 * request parameters in Map object
	 */

	@RequestMapping(value = "/auth_Map", method = RequestMethod.POST)
	public String authenticate(Map<String, Object> map) {
		System.out.println(map); // Output : {}

		String email = (String) map.get("email"); // email is null
		String password = (String) map.get("password"); // password is null

		if (email.equals(password)) // We will get java.lang.NullPointerException
			return "welcome";
		else
			return "failed";
	}

	/*
	 * In case of ModelAndView it is not possible to pass Model object from View to
	 * Controller
	 */

}
